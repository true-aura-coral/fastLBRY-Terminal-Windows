**Currently** FastLBRY login is not yet implement. But **it is possible to login**. 

While hacking on the [LBRY SDK](https://github.com/lbryio/lbry-sdk) I realized that it shares account information with any lbrynet running on the same system. So if you launch the LBRY Desktop app ones to log in, it will spread the login to all the other SDKs including FastLBRY. 

As soon as I will figure out how to make a login. I will implement it here, and this message will disappear. 
 
For now. Get [LBRY Desktop](https://lbry.com) login from there. And come back to here. You will be logged in.
