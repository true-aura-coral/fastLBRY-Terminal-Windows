# FastLBRY terminal

```
█▓▓█▓▓▓▓▓▓█▓██▓▓▓▓▓▓▓█▓▓▓▓▓▓█▓▓▓▓██▓▓▓▓█▓▓▓▓█
▓▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒▓▒▒▒▒▓▒▓
██░░░▒░░░░░░▒░░░░░░░▒░░░░░░░░░▒░░▒░░░░░░░░▒▓▓
▓▓░ ░          ░             ░    ■     ■░░▒▓
▓▒▒ ╔════════■   ░  ╔════╗ ╔════╗ ║  ░  ║ ░▓█
▓▒░ ║ ░      ║      ║    ║ ║    ║ ║     ║ ░▒▓
█▓░░║        ║      ╠════╣ ╠═╦══╝ ╚══╦══╝ ▒▒▓
▓▒▒ ╠══ AST ■║      ║    ║ ║ ╚══╗    ║   ░░▒█
█▒░ ║        ║      ║    ║ ║    ║    ║    ░▓▓
▓▓░ ║    ░   ╚═════■╚════╝ ■    ■ ░  ║ ░  ░▒▓
▓▒░░║ ░       THE TERMINAL CLIENT    ║    ▒▒█
█▒▒ ■      ░                  ░      ■ ▒░ ░▓▓
▓▒░░░░░░░▒░░░░▓░░░░░▒░░░░░░░░░▒░░░░▒░░░░░░░▒█
▓▓▒▒▒▒▓▒▒▒▒▒▓▒▒▒▒▓▒▒▓▒▒▒▒▒▒▒▓▓▒▒▒▒▒▒▓▒▒▒▒▒▓▓▓
█▓▓█▓▓▓▓▓████▓▓▓▓█▓▓▓▓█▓▓▓▓██▓▓▓█▓▓▓▓▓█▓▓▓▓██
```

This is one of the **3 programs** that will be the FastLBRY project. There are not enough choice when it comes to LBRY. You can either use the Odysee.com website. Or the LBRY Desktop that shares the [source code](https://github.com/lbryio/lbry-desktop) with Odysee, which makes it a sub-optimal solution. LBRY Desktop is more local, but it's still a [tiny browser](https://www.electronjs.org/) with a website on it. 

If you look on what's running on Odysee.com, you will discover a bloated mess of [compiled JavaScript](https://www.gnu.org/philosophy/javascript-trap.html) on top of other bloated mess of JavaScript. Pages barely contain HTML5 code. I've [talked to them about this](https://github.com/lbryio/lbry-desktop/issues/6197) and all they could say was:

> Thank you for the issue and write up. This is not something we plan to do anytime soon since we rely on the open-source videojs framework heavily. We may consider it in the future.

Fine. You won't do that. How about me? Maybe I can do that. It's Free / Libre Software we are talking about. You don't need a permission to start implementing things.

Here is the plan for this project:

 - **FastLBRY Terminal**. A fully featured, terminal application to interact with LBRY. It will allow watching videos, download files, view and send comments, upload new files. This stage is half finished. 
 - **FastLBRY GTK**. A fully featured graphical application. It will have all the same features as the FastLBRY Terminal but with a GUI that a basic user might understand. It will require a bit of UI design work. I don't want it to resemble YouTube. LBRY is way deeper then just a clone of YouTube. I want the design to showcase the LBRY network correctly.
 - **FastLBRY HTML5**. A fully featured server application. Released on the AGPL this time. That people could install to make instances of LBRY on the web. But instead of it being this bloated mess of JavaScript. It will be a straight forward HTML5 site. With JS maybe only in the most minimal form. And only for secondary features ( like live notifications for comments ). So it will not break core features if the user decides to block all JS.

# WE NEED HACKERS!!! WE NEED YOU!!!

This project requires a lot of work to be done. The good thing is. It's based on python. Without using too many complex features. We do use the [lbrynet SDK](https://github.com/lbryio/lbry-sdk) for this for now. Maybe we can implement the entire SDK in code later on.

If you know nothing about programming, this is a good way to start. If you know something about programming, this is a good opportunity to contribute. To learn things needed to hack on this project and to help it be where it needs to be you can use the following resources.

 - [Python Basics](https://pythonbasics.org/) to learn the basic syntax of python. You can use this as a handbook when ever you don't understand a line. Or when ever you need a way to implement a thing.
 - [LBRY SDK API](https://lbry.tech/api/sdk) to learn all kinds of possible things that you can do with the SDK. Alternatively you can go to the folder `flbry` ( using `cd flbry` ) and then use the `--help` feature to see various commands of the SDK. `./lbrynet --help`. This way I learned enough to hack this bare bones version so far.
 - [DuckDuckGo](https://duckduckgo.com/) a search engine that can help you find answers for things that are not documented in any of the previous places. Sometimes you may need to go to the second page.
 - [The Matrix Chat](https://app.element.io/#/room/#FastLBRY:matrix.org) (`#FastLBRY:matrix.org`) where you can hang out with already a quite substantial amount of users / hackers or FastLBRY. I'm also there. So if you have any question what so ever, just post it there.

**VIDEO TUTORIAL ON HOW TO HACK IT**

[![thumb](https://spee.ch/c/caef832cc7a1c169.png)](https://odysee.com/@blenderdumbass:f/hacking-on-fast-lbry:9)

*Click the image to watch on odysee or...* [Direct Spee.ch link](https://spee.ch/@blenderdumbass:f/hacking-on-fast-lbry:9)
 
Now open the `run.py` file in your [preferred editor](https://www.gnu.org/software/emacs/) and start hacking.
 
# To do list

To get an idea of what to work on, there is a checklist of things. Alternatively. You can look into the [Issues page](https://notabug.org/jyamihud/FastLBRY-terminal/issues) for various issues found by the users. That you can help fixing as well.

**Basic Features:**

 - [x] Search / List channel publications / See Trending
 - [x] Download / get https link / watch videos / read articles.
 - [x] Setup and upload new publications
 - [x] Read and write comments and replies
 - [x] See wallet history
 - [ ] Login *( currently requires to [login from LBRY Desktop](https://notabug.org/jyamihud/FastLBRY-terminal/src/master/help/login.md) )* [Issue #17](https://notabug.org/jyamihud/FastLBRY-terminal/issues/17)
 - [ ] Send support / send lbc to addresses / see balance [Issue #10](https://notabug.org/jyamihud/FastLBRY-terminal/issues/10), [Issue #15](https://notabug.org/jyamihud/FastLBRY-terminal/issues/15), [Issue #16](https://notabug.org/jyamihud/FastLBRY-terminal/issues/16)
 - [ ] View Following [Issue #13](https://notabug.org/jyamihud/FastLBRY-terminal/issues/13)

**Nice to have things**

 - [x] Make UI fit to the Terminal Size.
 - [ ] Changing the `lbrynet` binary to a full python implementation. [Issue #3](https://notabug.org/jyamihud/FastLBRY-terminal/issues/3)
 - [ ] Analytics [Issue #19](https://notabug.org/jyamihud/FastLBRY-terminal/issues/19)
 - [ ] Multi-Language support [Issue #21](https://notabug.org/jyamihud/FastLBRY-terminal/issues/21), [Issue #32](https://notabug.org/jyamihud/FastLBRY-terminal/issues/32)

# Licensing

The project is (c) J.Y.Amihud and Other Contributors 2021, under the [GNU General Public License Version 3](https://notabug.org/jyamihud/FastLBRY-terminal/src/master/LICENSE.md) or **any later version**. This choice will protect this project from proprietaryzation.

Unfortunately, the LBRY SDK is (c) LBRY Inc. and is under the [Expat License](https://www.gnu.org/licenses/license-list.html#Expat) ( also known as the MIT license. ) [Click here to read their license.](https://raw.githubusercontent.com/lbryio/lbry-sdk/master/LICENSE) Which wasn't our choice. You can complaint about it on the [LBRY SDK repository](https://github.com/lbryio/lbry-sdk).

# User help

If you have a question or an issue please tell us about it. We are not magical wizards. We can't read your mind. And we don't have telemetry. So it's on you to tell us about errors and other annoyances. You can use the:

 - [Issue tracker](https://notabug.org/jyamihud/FastLBRY-terminal/issues)
 - [Matrix Chat](https://app.element.io/#/room/#FastLBRY:matrix.org) (`#FastLBRY:matrix.org`)
